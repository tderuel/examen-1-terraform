# Examen-1 Terraform

CI / CD partie Ops - Par [Deruel Thomas](https://gitlab.com/tderuel)

## Tâches à effectuer

- [x] Ajout commit initial

- [x] Exécuter une validation local des fichiers Terraform

- [x] Vérifier le déploiement de l'infrastructure et stocker les fichiers `terraform.tfstate`

- [x] Supprimer l'infrastructure avec l'infrastructure
